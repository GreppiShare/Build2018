Greppi Build 2018
=======
[![link per il volantino del Build2018](website/static/VolantinoIISSGreppiBuild2018Page1.png)]( https://gitlab.com/GreppiShare/Build2018/raw/master/website/static/VolantinoIISSGreppiBuild2018.pdf "Volantino Download")

Cos'è il Greppi Build?
--------------------
Dal 2015 all'Istituto Alessandro Greppi di Monticello Brianza il Dipartimento di Informatica e Telecomunicazioni organizza la rassegna dei progetti sviluppati dai propri studenti dell'ultimo triennio. Questi progetti sono principalmente sviluppati nell'ambito dell'Alternanza Scuola Lavoro (ASL) e rivestono un'importanza fondamentale nell'impianto didattico dell'indirizzo. <br/>
La quasi totalità dei progetti sviluppati finora sono svolti in collaborazione con enti e aziende del territorio.
I progetti sono generalmente coordinati dai docenti delle materie di indirizzo (Informatica, Telecomunicazioni), ma talvolta trovano collaborazioni anche in altre discipline.
L’aspetto più importante di questi progetti è il fatto che sono svolti da gruppi di ragazzi con una grande passione per la tecnologia. Gli studenti sono al centro dell’attività di progetto e sviluppo: si organizzano in gruppi autonomi, definiscono compiti e tempistiche, valutano la qualità del prodotto sviluppato.


Alcuni dei progetti presentati al Greppi Build 2018
---------------------------------------------------
***

### ApertaMente

[![Link al sito di ApertaMente](website/static/MySmartOpinionIcon.png)]( https://gitlab.com/GreppiShare/ApertaMenteBuild "ApertaMente website")

[Sito del progetto ApertaMente][6]

La scheda descrittiva del progetto ApertaMente è scaricabile a questo [link][7]

[![Video spot del progetto AughTalk](website/static/MySmartOpinionVideoImage.png)](https://youtu.be/t9hSzeDbPis "Apertamente spot")
<br/>Video di Apertamente - lo spot
***


### AughTalk

[![Link al sito di AughTalk](website/static/aughtalk_icon.png)]( https://gitlab.com/GreppiShare/AughTalkBuild "AughTalk website")

[Sito del progetto AughTalk][2]

La scheda descrittiva del progetto AughTalk è scaricabile a questo [link][3]


[![Video del backstage del progetto AughTalk](website/static/BackStageVideoAughTalkImge.png)](https://youtu.be/RM39J4ezNEE "AughTalk BackStage")
<br/>Video di AughTalk - il backstage

[![Video spot del progetto AughTalk](website/static/VideoSpotAughTalkImage.png)](https://youtu.be/Lv_xiFYL4_8 "AughTalk spot")
<br/>Video di AughTalk - lo spot
***
### Smile On Air

[![Link al sito di Smile On Air](website/static/SmileOnAir_about.jpg)]( https://gitlab.com/GreppiShare/SmileOnAirBuild "Smile On Air website")

[Sito del progetto Smile On Air][4]

La scheda descrittiva del progetto Smile On Air è scaricabile a questo [link][5]


[![Video del backstage del progetto Smile On Air](website/static/SmileOnAirBackStage.png)](https://youtu.be/YEkwoVnDDn4 "Smile On Air BackStage")
<br/>Video di Smile On Air - il backstage

[![Video spot del progetto AughTalk](website/static/SmileOnAirSpot.png)](https://youtu.be/isD-Lb00A-Q "Smile On Air Spot")
<br/>Video di Smile On Air - lo spot

***


Per ulteriori informazioni sul progetto contattare l'[Istituto Alessandro Greppi][1] di Monticello Brianza (Lc)



Licenza
--------
    Copyright 2018 IISS "Alessandro Greppi" - Monticello Brianza (Lc),
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 [1]: http://www.issgreppi.gov.it
 [2]: https://gitlab.com/GreppiShare/AughTalkBuild
 [3]: https://gitlab.com/GreppiShare/AughTalkBuild/raw/master/website/static/Progetto_comunicatore_per_immagini-AughTalk.pdf
 [4]: https://gitlab.com/GreppiShare/SmileOnAirBuild
 [5]: https://gitlab.com/GreppiShare/SmileOnAirBuild/raw/master/website/static/Progetto_SmileOnAir.pdf
 [6]: https://gitlab.com/GreppiShare/ApertaMenteBuild
 [7]: https://gitlab.com/GreppiShare/ApertaMenteBuild/raw/master/website/static/ApertaMente.pdf
